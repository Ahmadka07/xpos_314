﻿using Microsoft.AspNetCore.Mvc;
using xpos314.web.Models;

namespace xpos314.web.Controllers
{
    public class FriendController : Controller
    {
        private static List<Friend> list = new List<Friend>()
            {
                new Friend(){ Id = 1, Name = "Ahmad", Address = "Ampera Raya" },
                new Friend(){ Id = 2, Name = "Khoirul", Address = "Gg.Rini" },
                new Friend(){ Id = 3, Name = "Anwar", Address = "Ragunan" },
            };

        public IActionResult Index()
        {
            Friend friend = new Friend();
            
            //friend.Id = 3;
            //friend.Name = "Isni";
            //friend.Address = "Cimahi";

            //list.Add(friend);

            ViewBag.listFriend = list;

            return View(list);
        }

        public IActionResult Detail(int id)
        {
            Friend friend = list.Find(a => a.Id == id)!;
            return View(friend);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Friend friend)
        {
            list.Add(friend);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            Friend friend = list.Find(a => a.Id == id)!;
            return View(friend);
        }

        [HttpPost]
        public IActionResult Edit(Friend data)
        {
            Friend friend = list.Find(a => a.Id == data.Id)!;
            int index = list.IndexOf(friend);
            
            if (index > -1)
            {
                list[index].Id = data.Id;
                list[index].Name = data.Name;
                list[index].Address = data.Address;
            }

            return RedirectToAction("Index");
        }

        //public IActionResult Delete(int id)
        //{
        //    Friend friend = list.Find(a => a.Id == id)!;
        //    return View(friend);
        //}

        //[HttpPost]
        //public IActionResult Delete(string id)
        //{
        //    Friend data = list.Find(a => a.Id == int.Parse(id))!;
        //    list.Remove(data);
        //    return RedirectToAction("Index");
        //}

        [HttpPost]
        [HttpGet]
        public IActionResult Delete(int id)
        {
            Friend friend = list.Find(a => a.Id == id)!;
            if (HttpContext.Request.Method == "POST")
            {
                list.Remove(friend);
                return RedirectToAction("Index");
            }
            else
            {
                return View(friend);
            }
        }
    }
}
