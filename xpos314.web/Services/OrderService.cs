﻿using Newtonsoft.Json;
using System.Text;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class OrderService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public OrderService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<VMResponse> SubmitOrder(VMOrderHeader dataHeader)
        {
            var json = JsonConvert.SerializeObject(dataHeader);
            var content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");


            var request = await client.PostAsync(RouteAPI + "apiOrder/SubmitOrder", content);

            
            if (request.IsSuccessStatusCode)
            {
                //Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke Object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<List<VMOrderHeader>> ListHeaderDetail(int IdCustomer)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiOrder/GetDataOrderHeaderDetail/{IdCustomer}");

            List<VMOrderHeader> listHeader = JsonConvert.DeserializeObject<List<VMOrderHeader>>(apiResponse)!;

            return listHeader;
        }

        public async Task<int> CountTransaction(int IdCustomer)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiOrder/CountTransaction/{IdCustomer}");

            int count = JsonConvert.DeserializeObject<int>(apiResponse);

            return count;
        }
    }
}
