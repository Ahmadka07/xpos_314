﻿using Newtonsoft.Json;
using System.Text;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class VariantService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public VariantService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<VMTblVariant>> GetAllData()
        {
            List<VMTblVariant> data = new List<VMTblVariant>();
            
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiVariant/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMTblVariant>>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Create(VMTblVariant dataParam)
        {
            //Proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //Proses mengubah string menjadi Json lalu di kirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //Proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiVariant/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke Object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMTblVariant> GetDataById(int id)
        {
            VMTblVariant data = new VMTblVariant();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiVariant/GetDataById/{id}");

            data = JsonConvert.DeserializeObject<VMTblVariant>(apiResponse)!;
            return data;
        }

        public async Task<List<VMTblVariant>> GetDataByIdCategory(int id)
        {
            List<VMTblVariant> data = new List<VMTblVariant>();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiVariant/GetDataByIdCategory/{id}");

            data = JsonConvert.DeserializeObject<List<VMTblVariant>>(apiResponse)!;
            return data;
        }

        public async Task<VMResponse> Edit(VMTblVariant dataParam)
        {
            //Proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //Proses mengubah string menjadi Json lalu di kirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //Proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiVariant/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke Object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiVariant/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                //Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke Object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
    }
}
